### Reinforcement algorithm evaluation 

The goal of the Project was to train multiple Reinforcement Models and compare their performance. The initial stage was a chess game with 3 figures. Then the Deep Sarsa, Deep Q-Learning and Deep Double Q-Learning was utilized to put the opponent to checkmate. 
